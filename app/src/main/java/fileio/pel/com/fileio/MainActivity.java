package fileio.pel.com.fileio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;


public class MainActivity extends AppCompatActivity {


    //this is a somple comment

    TextView textView;

    SharedPreferences sharedPreferences;

    int noOfFilesToKeep = 5;

    Handler handler;
    Runnable runnable;

    Runnable runnable2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sharedPreferences = getSharedPreferences("MYPREFS",MODE_PRIVATE);

        String username = sharedPreferences.getString("user","NULL");


        String name = "Sohail";
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user",name);
        editor.putBoolean("login",true);
        editor.commit();



        handler = new Handler();

         runnable = new Runnable() {
            @Override
            public void run() {

                handler.postDelayed(runnable,1000);
                Utils.printDLog(this.getClass().getSimpleName(),System.currentTimeMillis()+"");

            }
        };

        runnable2 = new Runnable() {
            @Override
            public void run() {


                //Utils.printDLog(this.getClass().getSimpleName(),System.currentTimeMillis()+"");
                createZip();

            }
        };


        handler.postDelayed(runnable,1000);
        handler.postDelayed(runnable2,10000);












    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }


    public void createZip(){



        ArrayList<Long> times = new ArrayList<>();
        File[] top5;



        if(isExternalStorageReadable() && isExternalStorageWritable()){

            Utils.printILog("isExternalStorageReadable&Writable","true");
        }else{

            Utils.printILog("isExternalStorageReadable&Writable","false");

            return;
        }

        //first we delete old zip file

        File zipDir = new File(Environment.getExternalStorageDirectory(
        ), "PEL");

        File[] zipfiles = zipDir.listFiles();
        for(File file:zipfiles){

            if(file.toString().contains(".zip")){

                Utils.printELog("Deleting old zip file first",file.toString());

                file.delete();
            }


        }



        File ergoDir = new File(Environment.getExternalStorageDirectory(
        ), "PEL");

        File[] afiles = ergoDir.listFiles();




        if(afiles.length > noOfFilesToKeep ){

            //now we proceed to deletion

            //first we sort them by date modified
            Arrays.sort(afiles, new Comparator<File>(){
                public int compare(File f1, File f2)
                {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }

            });

            for(File file:afiles){

                Log.e("file",file.toString());



            }


            top5 = Arrays.copyOfRange(afiles, afiles.length-noOfFilesToKeep,afiles.length);//Arrays.copyOfRange(afiles, 0,noOfFilesToKeep);
            //Arrays.copyOfRange(afiles, afiles.length-noOfFilesToKeep,afiles.length);


            System.out.println(Arrays.toString(top5));









        }else{

            top5 = afiles;

        }

        //now we delete old log files from the dir

        for(int i=0;i < afiles.length - noOfFilesToKeep ;i++){

            if(afiles[i].exists()){

                afiles[i].delete();

            }


        }

        String[] files = new String[top5.length];

        for(int i =0;i < top5.length;i++){




            files[i] = top5[i].toString();
        }



        String zipName = "file.zip";

        Compress c = new Compress(files,ergoDir.toString()+"/"+zipName); //ERGOLogs_timestamp.zip
        c.zip();

        sendEmail();


    }


    public void sendEmail(){

        String zipName = "file.zip";

        if(zipName.isEmpty()){

            Toast.makeText(getApplicationContext(),"Error creating zip file",Toast.LENGTH_SHORT).show();


        }


        File ergoDir = new File(Environment.getExternalStorageDirectory(
        ), "PEL");
        File logFile = new File(ergoDir, zipName);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");

        String[] emails = new String[]{"rizwan.ali@pelgroup.com.pk"};
        intent.putExtra(Intent.EXTRA_EMAIL, emails);
        intent.putExtra(Intent.EXTRA_SUBJECT, "ERGO Logs Report");
        String text = "email description";
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(logFile));

        startActivity(intent);



    }
}
