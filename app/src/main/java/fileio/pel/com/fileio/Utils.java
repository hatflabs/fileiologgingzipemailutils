package fileio.pel.com.fileio;

import android.util.Log;



/**
 * Created by apple on 7/19/17.
 */

public class Utils {


    public static  boolean INFO_LOG_ENABLED  = true;
    public static  boolean DEBUG_LOG_ENABLED  = true;
    public static  boolean ERROR_LOG_ENABLED  = true;
    public static  boolean FILE_LOG_ENABLED  = true;







    public static void printILog(String TAG,String message){

        if(INFO_LOG_ENABLED){

            Log.i(TAG,message);

        }

        if(FILE_LOG_ENABLED){


            Logger.addRecordToLog(TAG +": " + message + "\n");

        }

    }

    public static void printDLog(String TAG,String message){

        if(DEBUG_LOG_ENABLED){

            Log.d(TAG,message);

        }

        if(FILE_LOG_ENABLED){


            Logger.addRecordToLog(TAG +": " + message + "\n");

        }

    }
    public static void printELog(String TAG,String message){

        if(ERROR_LOG_ENABLED){

            Log.e(TAG,message);

        }

        if(FILE_LOG_ENABLED){


            Logger.addRecordToLog(TAG +": " + message + "\n");

        }

    }
}
